/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pasin.abstractanimal;

/**
 *
 * @author Pla
 */
public class TestAnmal {

    public static void main(String[] args) {
        Human h1 = new Human("Dang");
        Dog d1 = new Dog("Doom");
        Cat c1 = new Cat("Doy");

        LandAnimal[] landAnimals = {h1, d1, c1};
        for (int i = 0; i < landAnimals.length; i++) {
            landAnimals[i].eat();
            landAnimals[i].move();
            landAnimals[i].speak();
            landAnimals[i].sleep();
            landAnimals[i].run();
            System.out.println("==========================");
        }
        Crocodile cro1 = new Crocodile("Keyyyy");
        Snake s1 = new Snake("long");
        Reptile[] reptiles = {cro1, s1};

        for (int i = 0; i < reptiles.length; i++) {
            reptiles[i].eat();
            reptiles[i].move();
            reptiles[i].speak();
            reptiles[i].sleep();
            reptiles[i].crawl();
            System.out.println("==========================");
        }

        Bat b1 = new Bat("Battt");
        Bird bi1 = new Bird("Nokkkkk");

        Poultry[] poultrys = {b1, bi1};
        for (int i = 0; i < poultrys.length; i++) {
            poultrys[i].eat();
            poultrys[i].move();
            poultrys[i].speak();
            poultrys[i].sleep();
            poultrys[i].fly();
            System.out.println("==========================");
        }

        Fish f1 = new Fish("Pla");
        Crab crab1 = new Crab("Pu");
        AquaticAnimal[] aquatic = {f1, crab1};
        for (int i = 0; i < aquatic.length; i++) {
            aquatic[i].eat();
            aquatic[i].move();
            aquatic[i].speak();
            aquatic[i].sleep();
            aquatic[i].swim();
            System.out.println("==========================");
        }

        Plane plane = new Plane("Engine Number 1");
        Flyable[] flyables = {b1, bi1, plane};
        for (Flyable f : flyables) {
            if (f instanceof Plane) {
                Plane p = (Plane) f;
                p.stratEngine();
                p.run();
            }
            f.fly();
        }
        System.out.println("==========================");

        Car cars = new Car("Engine Number 2");
        Runable[] runables = {d1, c1, h1, cars};
        for (Runable r : runables) {
            if (r instanceof Car) {
                Car c = (Car) r;
                c.stratEngine();
            }
            r.run();
        }
        System.out.println("==========================");
    }
}
