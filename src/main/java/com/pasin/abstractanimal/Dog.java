/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pasin.abstractanimal;

/**
 *
 * @author Pla
 */
public class Dog extends LandAnimal {

    private String nickName;

    public Dog(String nickName) {
        super("Dog", 4);
        this.nickName = nickName;
    }

    @Override
    public void run() {
        System.out.println("Dog " + nickName + " run");
    }

    @Override
    public void eat() {
        System.out.println("Dog " + nickName + " eat");
    }

    @Override
    public void move() {
        System.out.println("Dog " + nickName + " walk");
    }

    @Override
    public void speak() {
        System.out.println("Dog " + nickName + " speak");
    }

    @Override
    public void sleep() {
        System.out.println("Dog " + nickName + " sleep");
    }

}
