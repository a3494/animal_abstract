/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pasin.abstractanimal;

/**
 *
 * @author Pla
 */
public class Snake extends Reptile {

    private String nickName;

    public Snake(String nickName) {
        super("Snake", 0);
        this.nickName = nickName;
    }

    @Override
    public void crawl() {
        System.out.println("Snake " + nickName + " crewl");
    }

    @Override
    public void eat() {
        System.out.println("Snake " + nickName + " eat");
    }

    @Override
    public void move() {
        System.out.println("Snake " + nickName + " Can't walk");
    }

    @Override
    public void speak() {
        System.out.println("Snake " + nickName + " speak");
    }

    @Override
    public void sleep() {
        System.out.println("Snake " + nickName + " sleep");
    }

}
