/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pasin.abstractanimal;

/**
 *
 * @author Pla
 */
public class Car extends Vahicle implements Runable {

    public Car(String engine) {
        super(engine);
    }

    @Override
    public void stratEngine() {
        System.out.println("Car : StratEngine");
    }

    @Override
    public void stopEngine() {
        System.out.println("Car : StopEngine");
    }

    @Override
    public void raiseSpeed() {
        System.out.println("Car : RaiseSpeed");
    }

    @Override
    public void applyBreak() {
        System.out.println("Car : ApplyBreak");
    }

    @Override
    public void run() {
        System.out.println("Car : Run");
    }

}
