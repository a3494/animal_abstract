/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pasin.abstractanimal;

/**
 *
 * @author Pla
 */
public class Cat extends LandAnimal {

    private String nickName;

    public Cat(String nickName) {
        super("Cat", 4);
        this.nickName = nickName;
    }

    @Override
    public void run() {
        System.out.println("Cat " + nickName + " run");
    }

    @Override
    public void eat() {
        System.out.println("Cat " + nickName + " eat");
    }

    @Override
    public void move() {
        System.out.println("Cat " + nickName + " walk ");
    }

    @Override
    public void speak() {
        System.out.println("Cat " + nickName + " speak");
    }

    @Override
    public void sleep() {
        System.out.println("Cat " + nickName + " sleep");
    }

}
