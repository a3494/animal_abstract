package com.pasin.abstractanimal;


/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author Pla
 */
public class Crocodile extends Reptile {

    private String nickName;

    public Crocodile(String nickName) {
        super("Crocodile", 4);
        this.nickName = nickName;
    }

    @Override
    public void crawl() {
        System.out.println("Crocodile " + nickName + " crewl");
    }

    @Override
    public void eat() {
        System.out.println("Crocodile " + nickName + " eat");
    }

    @Override
    public void move() {
        System.out.println("Crocodile " + nickName + " walk");
    }

    @Override
    public void speak() {
        System.out.println("Crocodile " + nickName + " speak");
    }

    @Override
    public void sleep() {
        System.out.println("Crocodile " + nickName + " sleep");
    }

}
