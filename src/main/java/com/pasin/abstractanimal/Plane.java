/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pasin.abstractanimal;

/**
 *
 * @author Pla
 */
public class Plane extends Vahicle implements Runable,Flyable{

    public Plane(String engine) {
        super(engine);
    }

    @Override
    public void stratEngine() {
        System.out.println("Plane : StartEngine");
    }

    @Override
    public void stopEngine() {
        System.out.println("Plane : StopEngine");   }

    @Override
    public void raiseSpeed() {
        System.out.println("Plane : RaiseSpeed");  }

    @Override
    public void applyBreak() {
        System.out.println("Plane : ApplyBreak");  }

    @Override
    public void run() {
       System.out.println("Plane : Run");  }

    @Override
    public void fly() {
       System.out.println("Plane : Fly");  }
    
}
