/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pasin.abstractanimal;

/**
 *
 * @author Pla
 */
public class Human extends LandAnimal {
   private String nickName;

    public Human(String nickName) {
        super("human",2);
        this.nickName=nickName;
    }

    @Override
    public void run() {
        System.out.println("Human " + nickName + " run");
    }

    @Override
    public void eat() {
        System.out.println("Human "+nickName+" eat");
    }

    @Override
    public void move() {
        System.out.println("Human "+nickName+" walk");
    }

    @Override
    public void speak() {
        System.out.println("Human "+nickName+" speak");
    }

    @Override
    public void sleep() {
        System.out.println("Human "+nickName+" sleep");
    }

}
